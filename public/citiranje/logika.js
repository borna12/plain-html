function provjeri() {
    if ($(".medium").val() == "knjiga") { $(".polje").hide(); $(".knjiga").show() }
    else if ($(".medium").val() == "casopis") { $(".polje").hide(); $(".casopis").show() }
    else if ($(".medium").val() == "zbornik") { $(".polje").hide(); $(".zbornik").show() }
    else if ($(".medium").val() == "kvalifikacijski") { $(".polje").hide(); $(".kvalifikacijski").show() }
    else if ($(".medium").val() == "enciklopedijska") { $(".polje").hide(); $(".enciklopedijska").show() }
    else if ($(".medium").val() == "mrezno") {
        provjeriMedij()
    }
}
provjeri()

function provjeriMedij() {
    if ($('input[name=izvor]:checked').val() == "CD-ROM") { $(".polje").hide(); $(".cd").show(); }
    else if ($('input[name=izvor]:checked').val() == "DVD-ROM") { $(".polje").hide(); $(".dvd").show(); }
    else if ($('input[name=izvor]:checked').val() == "Blu-ray disk") { $(".polje").hide(); $(".blu").show(); }
    else if ($('input[name=izvor]:checked').val() == "sajt") { $(".polje").hide(); $(".mrezno").show(); }
}

function copyClipboard(e) { if (document.body.createTextRange) (t = document.body.createTextRange()).moveToElementText(e), t.select(), document.execCommand("Copy"), t.removeAllRanges(); else if (window.getSelection) { var t, a = window.getSelection(); (t = document.createRange()).selectNodeContents(e), a.removeAllRanges(), a.addRange(t), document.execCommand("Copy"), a.removeAllRanges() } }



function ocisti() {
    $(".appnitro").find("input[type=text], textarea").val("");
}

function stvori_citat() {
    autor = $(".autor1").find("input").val();
    naslov = $(".naslov1").find("input").val();
    mjesto = $(".mjesto1").find("input").val();
    mjesto=mjesto.replace("-","–")
    godina = $(".godina1").find("input").val();
    izdavac = $(".izdavac1").find("input").val();
    izdavac=izdavac.replace("-","–")
    casopis = $(".casopis1").find("input").val();
    sveza = $(".svezak1").find("input").val();
    broj = $(".broj1").find("input").val();
    stranice = $(".stranice1").find("input").val();
    stranice=stranice.replace("-","–")
    poveznica = $(".poveznica1").find("input").val();
    urednik = $(".urednik1").find("input").val();
    zbornik = $(".zbornik1").find("input").val();
    skup = $(".skup1").find("input").val();
    websajt = $(".mrezno1").find("input").val();
    sveuciliste = $(".sveuciliste1").find("input").val();
    enciklopedija = $(".naziv_e1").find("input").val();
    natuknica = $(".natuknica1").find("input").val();
    //datum za poveznice
    if (poveznica == "" || poveznica == "http://") {
        poveznica = ""
    }
    else {
        vrijeme = new Date()
        t_godina = vrijeme.getFullYear()
        t_mjesec = vrijeme.getMonth()+1
        t_dan = vrijeme.getDate()
        if (t_mjesec == "1") { }
        else if (t_mjesec == "1") { t_mjesec = "siječnja" }
        else if (t_mjesec == "2") { t_mjesec = "veljače" }
        else if (t_mjesec == "3") { t_mjesec = "ožujka" }
        else if (t_mjesec == "4") { t_mjesec = "travnja" }
        else if (t_mjesec == "5") { t_mjesec = "svibnja" }
        else if (t_mjesec == "6") { t_mjesec = "lipnja" }
        else if (t_mjesec == "7") { t_mjesec = "srpnja" }
        else if (t_mjesec == "8") { t_mjesec = "kolovoza" }
        else if (t_mjesec == "9") { t_mjesec = "rujna" }
        else if (t_mjesec == "10") { t_mjesec = "listopada" }
        else if (t_mjesec == "11") { t_mjesec = "studenoga" }
        else if (t_mjesec == "12") { t_mjesec = "prosinca" }
        poveznica = poveznica + " (pristupljeno " + t_dan + ". " + t_mjesec + " " + t_godina + ".)"
    }

    //ako je više od tri autora
    if (autor.split(";").length >= 4) {
        autor = autor.split(";")
        autor = autor[0] + " i dr"
    }
    //za urednike
    if (urednik != "") {
        if (urednik.split(";").length >= 4) {
            urednik = urednik.split(";")
            urednik = "Ur. " + urednik[0] + " i dr"
        }
        else { urednik = "Ur. " + $(".urednik").find("input").val(); }
    }

    //generiranje za bibliografske jedinice
    if ($(".medium").val() == "knjiga") {
        redosljed = [autor, godina, "<em>" + naslov + "</em>", urednik, izdavac, mjesto]
        redosljed = redosljed.filter(function (e) { return e.replace(/(\r\n|\n|\r)/gm, "") });
        redosljed = redosljed.filter(function (elem) {
            return elem != "<em></em>";
        });
        redosljed = redosljed.join(". ")
        { $(".literatura").append("<li><span class='citati tooltip' onclick='copyClipboard(this)' title='kopirano u memoriju'>" + redosljed + ".</span></li>") }
    }
    else if ($(".medium").val() == "casopis") {

        if (sveza != "" && broj != "") {
            redosljed = [autor, godina, naslov, "<em>" + casopis + "</em> "+ sveza + "/" + broj, stranice, poveznica]
        }
        else if (sveza != "" || broj != ""){
            redosljed = [autor, godina, naslov, "<em>" + casopis + "</em> "+ sveza + broj, stranice, poveznica]
        }
        else{redosljed = [autor, godina, naslov, "<em>" + casopis + "</em>",sveza, broj, stranice, poveznica]}
        redosljed = redosljed.filter(function (e) { return e.replace(/(\r\n|\n|\r)/gm, "") });
        redosljed = redosljed.filter(function (elem) {
            return elem != "<em></em>";
        });
        redosljed = redosljed.join(". ")
        { $(".literatura").append("<li><span class='citati tooltip' onclick='copyClipboard(this)' title='kopirano u memoriju'>" + redosljed + ".</span></li>") }
    }

    else if ($(".medium").val() == "zbornik") {
        if (skup != "") {
            skup=" "+skup
        }
        redosljed = [autor, godina, naslov, "<em>" + zbornik + "</em>", skup, urednik, izdavac, mjesto, stranice, poveznica]
        redosljed = redosljed.filter(function (e) { return e.replace(/(\r\n|\n|\r)/gm, "") });
        redosljed = redosljed.filter(function (elem) {
            return elem != "<em></em>";
        });
        redosljed = redosljed.join(". ")
        { $(".literatura").append("<li><span class='citati tooltip' onclick='copyClipboard(this)' title='kopirano u memoriju'>" + redosljed + ".</span></li>") }
    }

    else if ($(".medium").val() == "mrezno") {
        if ($('input[name=izvor]:checked').val() == "sajt") {
            if (autor != "") {
                redosljed = [autor, godina, "<em>" + naslov + "</em>", websajt, poveznica]
            }
            else {
                redosljed = ["<em>" + naslov + "</em>", godina, websajt, poveznica]
            }        
        }
        else {
            if (autor != "") {
                redosljed = [autor, godina, "<em>" + naslov + "</em>", "[" + $('input[name=izvor]:checked').val() + "]", izdavac, mjesto]
            }
            else{
                redosljed = [ "<em>" + naslov + "</em>", godina, "[" + $('input[name=izvor]:checked').val() + "]", izdavac, mjesto]
            }
        }
        redosljed = redosljed.filter(function (e) { return e.replace(/(\r\n|\n|\r)/gm, "") });
        redosljed = redosljed.filter(function (elem) {
            return elem != "<em></em>";
        });
        redosljed = redosljed.join(". ")
        { $(".literatura").append("<li><span class='citati tooltip' onclick='copyClipboard(this)' title='kopirano u memoriju'>" + redosljed + ".</span></li>") }
    }
    else if ($(".medium").val() == "kvalifikacijski") {
        if (stranice != "") {
            redosljed = [autor, godina, "<em>" + naslov + "</em>", $('input[name=rad]:checked').val(), sveuciliste, mjesto, stranice +" str"]
        }
        else{
            redosljed = [autor, godina, "<em>" + naslov + "</em>", $('input[name=rad]:checked').val(), sveuciliste, mjesto]
        }
        redosljed = redosljed.filter(function (e) { return e.replace(/(\r\n|\n|\r)/gm, "") });
        redosljed = redosljed.filter(function (elem) {
            return elem != "<em></em>";
        });
        redosljed = redosljed.join(". ")
         $(".literatura").append("<li><span class='citati tooltip' onclick='copyClipboard(this)' title='kopirano u memoriju'>" + redosljed + ".</span></li>")
    }

    else if ($(".medium").val() == "enciklopedijska") {
        redosljed = [natuknica, godina, enciklopedija, izdavac,mjesto, poveznica]
        redosljed = redosljed.filter(function (e) { return e.replace(/(\r\n|\n|\r)/gm, "") });
        redosljed = redosljed.filter(function (elem) {
            return elem != "<em></em>";
        });
        redosljed = redosljed.join(". ")
         $(".literatura").append("<li><span class='citati tooltip' onclick='copyClipboard(this)' title='kopirano u memoriju'>" + redosljed + ".</span></li>")
    }
    // skrolaj doliterature
    if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();
        // Store hash
        var hash = "#literatura";
        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800, function () {

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
        });
    } // End if
    $('.tooltip').tooltipster({
        trigger: 'click',
        timer: "1"
    });
}

